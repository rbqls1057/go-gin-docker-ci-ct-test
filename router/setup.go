package router

import (
	"go-gin-docker/router/ping"

	"github.com/gin-gonic/gin"
)

func Setup(r *gin.Engine) {
	ping.Init(r)
}
