package ping

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Init(r *gin.Engine) {
	p := r.Group("/ping")
	p.GET("", func (c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "pong"})
	})
}
