package main

import (
	"fmt"
	"go-gin-docker/router"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Employee struct {
	ID int64 `gorm:"id"`
	FirstName string `gorm:"first_name"`
	LastName string `gorm:"last_name"`
	Phone string `gorm:"phone"`
	Email string `gorm:"email"`
}

type result struct {
	Name string `gorm:"name"`
}

func main() {
	r := gin.Default()
	dsn := "root:ghrja132!@@tcp(127.0.0.1:3306)/test"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	// var employee *Employee
	var result *result
	db.Table("employee e").Select("u.name").Joins("join nest_test.user u on e.id = u.user_id").Find(&result)
	fmt.Println(result)
	router.Setup(r)

	r.Run()
}
